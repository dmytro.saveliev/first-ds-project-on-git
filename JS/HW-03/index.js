let a;
let b;
let mathOp;

do {
  a = prompt("Input first number:");
} while (numCheck(a));

do {
  b = prompt("Input second number:");
} while (numCheck(b));

function numCheck(num) {
  return num === "" || Number.isNaN(+num);
}

mathOp = prompt("Input mathematical operator (+, -, *, /)");

const calc = function (a, b, mathOp) {
  switch (mathOp) {
    case "+":
      return +a + +b;

    case "-":
      return a - b;

    case "*":
      return a * b;

    case "/":
      return a / b;

    default:
      break;
  }
};
console.log(calc(a, b, mathOp));
