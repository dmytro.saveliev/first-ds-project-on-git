function createNewUser() {
  do {
    this.firstName = prompt("Input your name:");
  } while (this.firstName === "");
  do {
    this.lastName = prompt(this.firstName + ", input your surname:");
  } while (this.lastName === "");
  this.getLogin = function () {
    return (
      this.firstName.slice(0, 1).toLowerCase() + this.lastName.toLowerCase()
    );
  };
}
const newUser = new createNewUser();
console.log(newUser.getLogin());
console.log(newUser);
