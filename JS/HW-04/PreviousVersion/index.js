let name;
let secondName;

do {
  name = prompt("Input your Name:");
} while (textCheck(name));

do {
  secondName = prompt("Input your Second name:");
} while (textCheck(secondName));

function textCheck(text) {
  return text === "" || text === null;
}

function createNewUser(name, secondName) {
  const newUser = {
    firstName: name,
    lastName: secondName,
    getLogin: function () {
      return (
        this.firstName.slice(0, 1).toLowerCase() + this.lastName.toLowerCase()
      );
    },
  };
  console.log(newUser.getLogin());
  return newUser;
}

console.log(createNewUser(name, secondName));
