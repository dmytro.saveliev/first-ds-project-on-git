const myArray = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

function myArrayToHtml(myArrayList) {
  let newArray = myArrayList
    .map(function (elemArr) {
      elemArr = `<li>${elemArr}</li>`;
      return elemArr;
    })
    .join("");

  document.body.insertAdjacentHTML("afterbegin", `<ul>${newArray}</ul>`);
}

myArrayToHtml(myArray);
