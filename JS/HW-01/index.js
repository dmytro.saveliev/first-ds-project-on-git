let name = prompt("Enter your name, please:");

if (name === "" || name === null) {
  do {
    name = prompt(`Please enter your name! You have entered nothing`);
  } while (name === "" || name === null);
}
let age = prompt("Enter your age, please:");

if (isNaN(age) || age === "" || age === null) {
  do {
    age = prompt("Please enter your age as a number but not empty!");
  } while (isNaN(age) || age === "" || age === null);
}
let conf =
  age >= 18 && age <= 22 ? confirm("Are you sure you want to continue?") : "";

if (age < 18 || conf === false) {
  alert("You are not allowed to visit this website");
} else {
  alert(`Welcome, ${name}!`);
}
