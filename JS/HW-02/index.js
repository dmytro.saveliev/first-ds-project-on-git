let userNum = prompt("Give a number please:");
while (
  Number.isNaN(+userNum) ||
  userNum === "" ||
  userNum === null ||
  !Number.isInteger(+userNum)
) {
  userNum = prompt(
    "You entered not a number, nothing or not an Integer. \nGive a number please:"
  );
}
for (i = 0; i <= userNum; i = i + 1) {
  if (i % 5 !== 0) {
    continue;
  }
  console.log(i);
}
if ((userNum < 5 && userNum > 1) || (userNum > -5 && userNum < -1)) {
  console.log("Sorry, no numbers");
}
