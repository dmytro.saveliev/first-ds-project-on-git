const arrData = ["hello", [], null, "world", {}, 23, false, "23", null, true];
const arrTypes = ["string", "number", "boolean", "object", "null"];

function filterBy(data, type) {
  if (type === "null") {
    type = "object";
  }

  const arrFilter = data.filter(function (element) {
    return typeof element !== type;
  });
  return arrFilter;
}

arrTypes.forEach(function (element) {
  return console.log(filterBy(arrData, element));
});
