function CreateNewUser() {
  do {
    this.firstName = prompt("What's your name:");
  } while (this.firstName === "");
  do {
    this.lastName = prompt(this.firstName + ", what's your surname:");
  } while (this.lastName === "");
  this.getLogin = function () {
    return (
      this.firstName.slice(0, 1).toLowerCase() + this.lastName.toLowerCase()
    );
  };

  this.birthday = prompt("What's your birthday?", "06.08.1985");
  let arrBirth = this.birthday.split(".");
  let arrBirthRev = arrBirth.reverse();
  let birthRev = new Date(arrBirthRev.join("."));

  this.getAge = function () {
    let now = new Date();
    if (birthRev.getMonth() > now.getMonth()) {
      return now.getFullYear() - birthRev.getFullYear() - 1;
    } else {
      return now.getFullYear() - birthRev.getFullYear();
    }
  };

  this.getPassword = function () {
    return (
      this.firstName.slice(0, 1).toUpperCase() +
      this.lastName.toLocaleLowerCase() +
      birthRev.getFullYear()
    );
  };
}
const newUser = new CreateNewUser();
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());
console.log(newUser);
